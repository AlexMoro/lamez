module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    // let us know if our JS is sound
    jshint: {
      options: {
        "bitwise": true,
        "browser": true,
        "curly": true,
        "eqeqeq": true,
        "eqnull": true,
        "es5": false,
        "esnext": true,
        "immed": true,
        "jquery": true,
        "latedef": true,
        "newcap": true,
        "noarg": true,
        "node": true,
        "strict": false,
        "trailing": true,
        "undef": true,
        "globals": {
          "jQuery": true,
          "alert": true,
          "lity": true,
          "Modernizr": true
        }
      },
      all: [
      'Gruntfile.js',
      'template/assets/js/source/options.js'
      ]
    },

    multi_lang_site_generator: {
      default: {
        options: {
          vocabs:           ['ca', 'fr'],
          vocab_directory:  'template/txt',
          output_directory: 'template/dev/'
        },
        files: {
          'lp.html': ['template/lp.tmpl']
        }
      }
    },

    // concatenation and minification all in one
    uglify: {
      dist: {
        files: {
          'template/dev/js/vendor.min.js': [
          'template/assets/js/vendor/jquery/jquery-1.11.1.js',
          'template/assets/js/vendor/modernizr/modernizr-build.js',
          'template/assets/js/vendor/jquery/jquery-ui.min.js'
          ],
          'template/dev/js/script.min.js': [
            'template/assets/js/vendor/inview/inview.js',
            'template/assets/js/vendor/slick/slick.js',
            'template/assets/js/source/options.js'
          ]
        }
      }
    },

    // style (Sass) compilation via Compass
    compass: {
      dist: {
        options: {
          sassDir: 'template/assets/css/source',
          cssDir: 'template/dev/css',
          imagesDir: 'images',
          images: 'images',
          javascriptsDir: 'template/dev/js',
          environment: 'production',
          outputStyle: 'compressed',
          relativeAssets: true,
          noLineComments: true,
          drop_console: false,
          force: true
        }
      }
    },

    // watch our project for changes
    watch: {
      options: {
        livereload: true,
      },
      compass: {
        files: [
        'template/assets/css/source/**/*',
        'template/*.tmpl',
        'template/txt/*.json',
        'template/dev/css/**/*',
        'template/assets/uncompressed-images/**/*'
        ],
        tasks: ['compass', 'uglify', 'multi_lang_site_generator']
      },
      js: {
        files: [
        '<%= jshint.all %>'
        ],
        tasks: ['jshint', 'uglify']
      }
    },
    imagemin: {
       dist: {
          options: {
            optimizationLevel: 7
          },
          files: [{
             expand: true,
             cwd: 'template/assets/uncompressed-images',
             src: ['**/*.{png,svg,jpg,webp,gif}'],
             dest: 'template/dev/images'
          }]
       }
    },
    cwebp: {
      images: {
        options: {
          arguments: [ '-q', 90 ],
          concurrency: 10
        },
        files: [
          { src: [ 'template/assets/uncompressed-images/x0/**/*.jpg', 'template/assets/uncompressed-images/x0/**/*.png' ] }
        ]
      }
    },
    copy: {
      main: {
        options: {
          process: function (content, srcpath) {
            content = content.replace(/<script src="\/\/localhost:35729\/livereload.js"><\/script>/g, "");
            content = content.replace(/\.\.\/css\/style.css/g,"content/homepage/2017/170808_CreatingAnIcon/css/style.css");
            content = content.replace(/\.\.\/js\/script.min.js/g,"content/homepage/2017/170808_CreatingAnIcon/js/script.min.js");
            content = content.replace(/\.\.\/images/g,"content/homepage/2017/170808_CreatingAnIcon/images");
            content = content.replace(/\.\.\/videos/g,"content/homepage/2017/170808_CreatingAnIcon/videos");
            content = content.replace(/\.svg/g,".svg?$staticlink$");
            content = content.replace(/\.jpg/g,".jpg?$staticlink$");
            content = content.replace(/\.webp/g,".webp?$staticlink$");
            content = content.replace(/\.png/g,".png?$staticlink$");
            content = content.replace(/\.gif/g,".gif?$staticlink$");
            content = content.replace(/\.css/g,".css?$staticlink$");
            content = content.replace(/\.js/g,".js?$staticlink$");
            content = content.replace(/<html lang="en">/g, "");
            content = content.replace(/<\/html>/g, "");
            content = content.replace(/<body>/g, "");
            content = content.replace(/<\/body>/g, "");
            content = content.replace(/<head>/g, "");
            content = content.replace(/<\/head>/g, "");
            content = content.replace(/<!doctype html>/g, "");
            content = content.replace(/<meta charset="utf-8">/g, "");
            content = content.replace(/<meta name="viewport" content="width=device-width, initial-scale=1">/g, "");
            content = content.replace(/<title>MASTER BUILD<\/title>/g, "");
            return content;
          },
        },
        files: [
            {
                expand: true,
                cwd   : 'template/dev/',
                src: ['{ca,fr,us}/*'],
                dest  : 'template/build/'
            }
        ]
      },
    }
  });

  // load tasks
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-multi-lang-site-generator');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-webp-compress');


  // register task
  grunt.registerTask('compile', [
    'multi_lang_site_generator',
    'imagemin'
  ]);
  grunt.registerTask('webp', [
    'cwebp'
  ]);
  grunt.registerTask('resize', [
    'image_resize'
  ]);
  grunt.registerTask('dev', [
  'jshint',
  'compass',
  'uglify',
  'watch'
  ]);
  grunt.registerTask('build', [
    'copy'
  ]);
};
