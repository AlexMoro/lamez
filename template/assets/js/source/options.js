/*
LesManifestes
26/09/17
*/

$('#lp-slider').not('.slick-initialized').slick({
  dots: false,
  infinite: false,
  speed: 500,
  autoplaySpeed: 4000,
  autoplay: false,
  pauseOnHover: false,
  slidesToShow: 3,
  slidesToScroll: 1,
  arrows: true,
  nextArrow: '<a class="btn-arrow next"><svg id="arrows" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 39.2 32.5"><style>.st0{fill:#fff}</style><switch><g><path class="st0" d="M36.8 16.8h-8c-3.2 0-3.2 5 0 5h8c3.2 0 3.2-5 0-5z"/><path class="st0" d="M24.1 27.9c-2.8-1.3-19.5-9.1-18.5-11.8.8-2.1 7.9-4.9 9.9-6 3.6-1.9 7.3-3.6 11-5.1 2.9-1.2 1.6-6.1-1.3-4.8C19.7 2.5 14.3 5.1 9 8 6 9.7.4 11.9 0 16c-.3 3.6 3.4 5.6 5.9 7.3 5 3.5 10.2 6.4 15.7 8.9 2.9 1.4 5.4-2.9 2.5-4.3z"/></g></switch></svg></a>',
  prevArrow: '<a class="btn-arrow prev"><svg id="arrows" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 39.2 32.5"><style>.st0{fill:#fff}</style><switch><g><path class="st0" d="M36.8 16.8h-8c-3.2 0-3.2 5 0 5h8c3.2 0 3.2-5 0-5z"/><path class="st0" d="M24.1 27.9c-2.8-1.3-19.5-9.1-18.5-11.8.8-2.1 7.9-4.9 9.9-6 3.6-1.9 7.3-3.6 11-5.1 2.9-1.2 1.6-6.1-1.3-4.8C19.7 2.5 14.3 5.1 9 8 6 9.7.4 11.9 0 16c-.3 3.6 3.4 5.6 5.9 7.3 5 3.5 10.2 6.4 15.7 8.9 2.9 1.4 5.4-2.9 2.5-4.3z"/></g></switch></svg></a>',
  centerMode: false,
  fade: false,
  cssEase: 'linear',
  responsive: [{
        breakpoint: 800,
        settings: {
            slidesToShow: 1
        }
    }]
});

/***********************************************
* Date checker to display eventDate > todayDate
************************************************/
$(".slick-slide").each(function(){

  function compareDate(todaysDate, inputDate) {
      var dateOne = new Date(todaysDate);
      var dateTwo = new Date(inputDate);
      if (dateOne <= dateTwo) {
        $(this).addClass('valid-date');
       }else {
         var i = $(this).attr('data-slick-index');
         console.log(i);
         $('#lp-slider').slick('slickRemove', i);
       }
   }

   var eventDate = $(this).attr('data-date');
   var todaysDate = new Date();
   var inputDate = new Date(eventDate);

   compareDate(todaysDate, inputDate);

   //filter to create

  /*
  if(inputDate.setHours(0,0,0,0) >= todaysDate.setHours(0,0,0,0)) {
    $(this).addClass('valid-date');
    console.log(eventDate);
  }else{
    var i = $(this).attr('data-slick-index');
    $('#lp-slider').slick('slickRemove', i);
  }
  */

});
